# MapReduce Application #

 
* tags-count (usage of Combiner and DistributedCache)
* spends-and-visits (usage of SequenceFileFormat + Snappy compression)
* browser-counters (usage of counters)
* secondary-sort with max site impressions 

# Tests with mr-unit included. #

# To setup and execute application follow next steps: #

1) Create mapred user in hdfs:
```
#!bash
su hdfs -c "hdfs dfs -mkdir /user/mapred"
su hdfs -c "hdfs dfs -chown mapred /user/mapred"
```
2) Go to application home directory and execute:
```
#!bash
mvn clean package
```
3) Copy ${application_home}/target/mapreduce-0.0.1.jar and ${application_home}/src/main/resources/res.csv (file from hw2) to /opt directory:
```
#!bash
cp ${application_home}/target/mapreduce-0.0.1.jar /opt
cp ${application_home}/src/main/resources/res.csv /opt
```
4) Execute tag-counts (task1 hw3):
```
#!bash
su - mapred -c "hadoop jar /opt/mapreduce-0.0.1.jar home.nkavtur.mapreduce.tags_count.JobClient -files /opt/res.csv ${pathToDatasetInHdfs} /user/mapred/tags-count"
```
5) Execute spends-and-visits (task2 hw3):
```
#!bash
su - mapred -c "hadoop jar /opt/mapreduce-0.0.1.jar home.nkavtur.mapreduce.spends_and_visits.JobClient ${pathToDatasetInHdfs} /user/mapred/spends-and-visits"
```
6) Execute browser-counters (task3 hw3):
```
#!bash
su - mapred -c "hadoop jar /opt/mapreduce-0.0.1.jar home.nkavtur.mapreduce.browser_counts.JobClient ${pathToDatasetInHdfs}"
```
7) Execute secondary sort (task from hw4). Reducer adds max site impressions iPinyouId to the end of the file. Only one reducer is required here
```
#!bash
# run sort
su - mapred -c "hadoop jar /opt/mapreduce-0.0.1.jar home.nkavtur.mapreduce.secondary_sort.JobClient ${pathToDatasetInHdfs} /user/mapred/secondary-sort"
# view max ipinyouid
hdfs dfs -tail /user/mapred/secondary-sort/part-r-00000 | tail -n1
```