package home.nkavtur.mapreduce.browser_counters;

import static home.nkavtur.mapreduce.utils.Constants.FIELDS_NUMBER;
import static home.nkavtur.mapreduce.utils.Constants.TAB;
import static home.nkavtur.mapreduce.utils.Constants.USER_AGENT_FIELD_NUMBER;
import static java.lang.String.format;

import java.io.IOException;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Logger;

/**
 * @author nkavtur
 */
public class BrowserCountersMapper extends Mapper<Object, Text, NullWritable, NullWritable>{
    private static final Logger LOG = Logger.getLogger(BrowserCountersMapper.class);

    @Override
    protected void map(Object key, Text value, Mapper<Object, Text, NullWritable, NullWritable>.Context context)
            throws IOException, InterruptedException {
        String[] tokens = value.toString().split(TAB);

        if (isNotValid(tokens)) {
            LOG.info(format("Line [%s] is not valid. Has size of [%s] but [%s]", value, tokens.length, FIELDS_NUMBER));
            return;
        }

        String userAgent = tokens[USER_AGENT_FIELD_NUMBER].toString().toUpperCase();
        if (isFirefox(userAgent)) {
            LOG.debug(format("Firefox detected for userAgent [%s]", userAgent));
            context.getCounter(BrowserType.FIREFOX).increment(1);
            return;
        }
        if (isChrome(userAgent)) {
            LOG.debug(format("Chrome detected for userAgent [%s]", userAgent));
            context.getCounter(BrowserType.CHROME).increment(1);
            return;
        }
        if (isIE(userAgent)) {
            LOG.debug(format("IE detected for userAgent [%s]", userAgent));
            context.getCounter(BrowserType.IE).increment(1);
            return;
        }

        LOG.debug(format("Unknown browser for userAgent [%s]", userAgent));
        context.getCounter(BrowserType.OTHERS).increment(1);
    }

    private boolean isNotValid(String[] tokens) {
        return tokens.length != FIELDS_NUMBER;
    }

    private boolean isFirefox(String userAgent) {
        return userAgent.toUpperCase().contains("FIREFOX");
    }

    private boolean isChrome(String userAgent) {
        return userAgent.toUpperCase().contains("CHROME");
    }

    private boolean isIE(String userAgent) {
        return userAgent.toUpperCase().contains("MSIE");
    }
}
