package home.nkavtur.mapreduce.browser_counters;

public enum BrowserType {
    FIREFOX, IE, CHROME, OTHERS
}
