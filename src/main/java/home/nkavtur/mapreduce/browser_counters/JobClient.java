package home.nkavtur.mapreduce.browser_counters;

import static java.lang.String.format;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

public class JobClient extends Configured implements Tool {
    private static final Logger LOG = Logger.getLogger(JobClient.class);
    private static final String APP_NAME = "Browser-Counts";

    @Override
    public int run(String[] args) throws Exception {
        LOG.info("Setupping job");
        Job job = Job.getInstance(getConf());
        job.setJarByClass(this.getClass());
        job.setJobName(APP_NAME);

        job.setMapperClass(BrowserCountersMapper.class);
        job.setMapOutputKeyClass(NullWritable.class);
        job.setMapOutputValueClass(NullWritable.class);
        job.setNumReduceTasks(0);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        job.setOutputFormatClass(NullOutputFormat.class);

        int exitCode = job.waitForCompletion(true) ? 1 : 0;

        LOG.info(format("Firefox detected [%s] times ", job.getCounters().findCounter(BrowserType.FIREFOX).getValue()));
        LOG.info(format("Chrome detected [%s] times ", job.getCounters().findCounter(BrowserType.CHROME).getValue()));
        LOG.info(format("IE detected [%s] times ", job.getCounters().findCounter(BrowserType.IE).getValue()));
        LOG.info(format("Others detected [%s] times ", job.getCounters().findCounter(BrowserType.OTHERS).getValue()));

        return exitCode;
    }

    public static void main(final String[] args) throws Exception {
        int exitCode = ToolRunner.run(new Configuration(), new JobClient(), args);
        System.exit(exitCode);
    }
}