package home.nkavtur.mapreduce.secondary_sort;

import static org.apache.commons.lang.builder.ToStringStyle.SHORT_PREFIX_STYLE;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.hadoop.io.WritableComparable;

/**
 * @author nkavtur
 */
public class CompositeKey implements WritableComparable<CompositeKey> {
    private String iPinyouId;
    private long timestamp;

    public CompositeKey() {}

    public CompositeKey(String iPinyouId, long timestamp, boolean siteImpression) {
        this.iPinyouId = iPinyouId;
        this.timestamp = timestamp;
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        iPinyouId = in.readUTF();
        timestamp = in.readLong();
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeUTF(iPinyouId);
        out.writeLong(timestamp);
    }

    @Override
    public int hashCode() {
        return this.iPinyouId.hashCode();
    }

    public String getIPinyouId() {
        return iPinyouId;
    }

    public CompositeKey withIPinyouId(String iPinyouId) {
        this.iPinyouId = iPinyouId;
        return this;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public CompositeKey withTimestamp(long timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, SHORT_PREFIX_STYLE);
    }

    @Override
    public int compareTo(CompositeKey key2) {
        int iPinyouIdComp = this.getIPinyouId().compareToIgnoreCase(key2.getIPinyouId());
        if (iPinyouIdComp != 0) {
            return iPinyouIdComp;
        }

        return (int) Math.signum(this.getTimestamp() - key2.getTimestamp());
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
}
