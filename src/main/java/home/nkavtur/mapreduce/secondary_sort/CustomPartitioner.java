package home.nkavtur.mapreduce.secondary_sort;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * @author nkavtur
 */
public class CustomPartitioner extends Partitioner<CompositeKey, Text> {
    @Override
    public int getPartition(CompositeKey key, Text value, int numPartitions) {
        return (key.getIPinyouId().hashCode() & Integer.MAX_VALUE) % numPartitions;
    }

    /**
     * @author nkavtur
     */
    public static class TimestampComparator extends WritableComparator {
        public TimestampComparator() {
            super(CompositeKey.class, true);
        }

        @Override
        @SuppressWarnings("rawtypes")
        public int compare(WritableComparable a, WritableComparable b) {
            CompositeKey key1 = (CompositeKey) a;
            CompositeKey key2 = (CompositeKey) b;

            int iPinyouIdComp = key1.getIPinyouId().compareToIgnoreCase(key2.getIPinyouId());
            if (iPinyouIdComp != 0) {
                return iPinyouIdComp;
            }

            return (int) Math.signum(key1.getTimestamp() - key2.getTimestamp());
        }
    }

    /**
     * @author nkavtur
     */
    public static class IpinyouIdGroupingComparator extends WritableComparator {
        public IpinyouIdGroupingComparator() {
            super(CompositeKey.class, true);
        }

        @Override
        @SuppressWarnings("rawtypes")
        public int compare(WritableComparable a, WritableComparable b) {
            CompositeKey key1 = (CompositeKey) a;
            CompositeKey key2 = (CompositeKey) b;
            return key1.getIPinyouId().compareToIgnoreCase(key2.getIPinyouId());
        }
    }

}
