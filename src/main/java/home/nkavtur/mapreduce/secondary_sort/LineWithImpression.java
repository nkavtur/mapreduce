package home.nkavtur.mapreduce.secondary_sort;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.hadoop.io.Writable;

/**
 * @author nkavtur
 */
public class LineWithImpression implements Writable {
    private String line;
    private boolean siteImpression;

    public LineWithImpression() {};

    public LineWithImpression(String line, boolean siteImpression) {
        this.line = line;
        this.siteImpression = siteImpression;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeUTF(line);
        out.writeBoolean(siteImpression);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        line = in.readUTF();
        siteImpression = in.readBoolean();
    }

    public String getLine() {
        return line;
    }

    public LineWithImpression withLine(String line) {
        this.line = line;
        return this;
    }

    public boolean isSiteImpression() {
        return siteImpression;
    }

    public LineWithImpression withSiteImpression(boolean siteImpression) {
        this.siteImpression = siteImpression;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
}
