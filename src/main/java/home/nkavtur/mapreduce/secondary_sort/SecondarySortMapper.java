package home.nkavtur.mapreduce.secondary_sort;

import static home.nkavtur.mapreduce.utils.Constants.*;
import static home.nkavtur.mapreduce.utils.Constants.STREAM_ID_FIELD_NUMBER;
import static home.nkavtur.mapreduce.utils.Constants.IPINYOUID_FIELD_NUMBER;
import static home.nkavtur.mapreduce.utils.Constants.TAB;
import static home.nkavtur.mapreduce.utils.Constants.TIMESTAMP_FIELD_NUMBER;
import static java.lang.String.format;
import static org.apache.hadoop.util.StringUtils.equalsIgnoreCase;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Logger;

/**
 * @author nkavtur
 */
public class SecondarySortMapper extends Mapper<Object, Text, CompositeKey, LineWithImpression> {
    private static final Logger LOG = Logger.getLogger(SecondarySortMapper.class);

    @Override
    protected void map(Object key, Text line, Context context) throws IOException, InterruptedException {
        LOG.debug(format("Processing line [%s]", line));

        String[] tokens = line.toString().split(TAB);
        if (isNotValid(tokens)) {
            LOG.debug(format("Skipping line [%s]. Has size of [%s], but should [%s]", line, tokens.length, FIELDS_NUMBER));
            return;
        }

        String iPinyouId = tokens[IPINYOUID_FIELD_NUMBER];
        long timestamp = tryParseLong(tokens[TIMESTAMP_FIELD_NUMBER]);
        int streamId = tryParseInt(tokens[STREAM_ID_FIELD_NUMBER]);

        if (equalsIgnoreCase(iPinyouId, "null")) {
            LOG.debug(format("Skipping line [%s]. iPinyou null", line));
            return;
        }

        CompositeKey compositeKey = new CompositeKey().withIPinyouId(iPinyouId).withTimestamp(timestamp);
        LineWithImpression lineWithImpression = new LineWithImpression().
                withLine(line.toString()).
                withSiteImpression(streamId == IMPRESSION_SITE_VALUE);
        LOG.debug(format("Emiting key=[%s], value=[%s]", compositeKey, lineWithImpression));
        context.write(compositeKey, lineWithImpression);
    }

    private boolean isNotValid(String[] tokens) {
        return tokens.length != FIELDS_NUMBER;
    }

    private long tryParseLong(String timestamp) {
        try {
            return Long.parseLong(timestamp);
        } catch(NumberFormatException e) {
            LOG.debug(format("Timestamp=[%s] is invalid", timestamp));
            return 0L;
        }
    }

    private int tryParseInt(String streamId) {
        try {
            return Integer.parseInt(streamId);
        } catch(NumberFormatException e) {
            LOG.debug(format("Timestamp=[%s] is invalid", streamId));
            return 0;
        }
    }
}
