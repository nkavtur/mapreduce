package home.nkavtur.mapreduce.secondary_sort;

import static java.lang.String.format;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.log4j.Logger;

/**
 * @author nkavtur
 */
public class SecondarySortReducer extends Reducer<CompositeKey, LineWithImpression, Text, Text> {
    private static final Logger LOG = Logger.getLogger(SecondarySortReducer.class);
    private String maxImpressionIPinyouId;
    private long currentSumImpressionMax;

    @Override
    protected void reduce(CompositeKey key, Iterable<LineWithImpression> values, Context context)
            throws IOException, InterruptedException {
        long siteImpressionsSum = 0;
        for (LineWithImpression value: values) {
            if (value.isSiteImpression()) siteImpressionsSum++;
            LOG.debug(format("Reducer received key=[%s], value=[%s]", key, value.getLine()));
            context.write(new Text(value.getLine()), new Text());
        }

        if (siteImpressionsSum > currentSumImpressionMax) {
            maxImpressionIPinyouId = key.getIPinyouId();
            currentSumImpressionMax = siteImpressionsSum;
        }
    }

    @Override
    protected void cleanup(Reducer<CompositeKey, LineWithImpression, Text, Text>.Context context)
            throws IOException, InterruptedException {
        context.write(new Text(maxImpressionIPinyouId), new Text(String.valueOf(currentSumImpressionMax)));
    }
}
