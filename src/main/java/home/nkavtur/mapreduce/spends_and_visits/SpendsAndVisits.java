package home.nkavtur.mapreduce.spends_and_visits;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;

/**
 * @author nkavtur
 */
public class SpendsAndVisits implements Writable {
    private LongWritable spends = new LongWritable();
    private LongWritable visits = new LongWritable();

    public SpendsAndVisits() {}

    public static SpendsAndVisits newInstance() {
        return new SpendsAndVisits();
    }

    public SpendsAndVisits(LongWritable spends, LongWritable visits) {
        this.spends = spends;
        this.visits = visits;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        spends.write(out);
        visits.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        spends.readFields(in);
        visits.readFields(in);
    }

    public LongWritable getVisits() {
        return visits;
    }

    public SpendsAndVisits withVisits(LongWritable visits) {
        this.visits = visits;
        return this;
    }

    public LongWritable getSpends() {
        return spends;
    }

    public SpendsAndVisits withSpends(LongWritable spends) {
        this.spends = spends;
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
        
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }
}
