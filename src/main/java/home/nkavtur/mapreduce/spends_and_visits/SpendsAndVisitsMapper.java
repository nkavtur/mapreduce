package home.nkavtur.mapreduce.spends_and_visits;

import static home.nkavtur.mapreduce.utils.Constants.FIELDS_NUMBER;
import static home.nkavtur.mapreduce.utils.Constants.IP_NUMBER;
import static home.nkavtur.mapreduce.utils.Constants.PRICE_NUMBER;
import static home.nkavtur.mapreduce.utils.Constants.TAB;
import static java.lang.String.format;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Logger;

/**
 * @author nkavtur
 */
public class SpendsAndVisitsMapper extends Mapper<Object, Text, Text, SpendsAndVisits> {
    private static final Logger LOG = Logger.getLogger(SpendsAndVisitsMapper.class);
    private static final LongWritable ONE = new LongWritable(1);

    @Override
    protected void map(Object key, Text line, Context context) throws IOException, InterruptedException {
        LOG.debug(format("Processing line [%s]", line));

        String[] tokens = line.toString().split(TAB);
        if (isNotValid(tokens)) {
            LOG.info(format("Line [%s] is not valid. Has size of [%s] but [%s]", line, tokens.length, FIELDS_NUMBER));
            return;
        }

        String ip = tokens[IP_NUMBER];
        long price = tryParse(tokens[PRICE_NUMBER]);

        SpendsAndVisits spendAndVisits = new SpendsAndVisits().withSpends(new LongWritable(price)).withVisits(ONE);
        LOG.debug(format("Emiting key=[%s], value=[%s]", ip, spendAndVisits));
        context.write(new Text(ip), spendAndVisits);
    }

    private boolean isNotValid(String[] tokens) {
        return tokens.length != FIELDS_NUMBER;
    }

    private long tryParse(String price) {
        try {
            return Long.parseLong(price);
        } catch(Exception e) {
            LOG.info(format("Price=[%s] is invalid", price));
            return 0L;
        }
    }
}
