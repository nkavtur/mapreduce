package home.nkavtur.mapreduce.spends_and_visits;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * @author nkavtur
 */
public class SpendsAndVisitsReducer extends Reducer<Text, SpendsAndVisits, Text, SpendsAndVisits> {
    @Override
    protected void reduce(Text key, Iterable<SpendsAndVisits> values, Context context)
            throws IOException, InterruptedException {
        long totalSpends = 0, totalVisits = 0;
        for (SpendsAndVisits value: values) {
            totalSpends += value.getSpends().get();
            totalVisits += value.getVisits().get();
        }

        context.write(key, new SpendsAndVisits().withSpends(new LongWritable(totalSpends)).withVisits(new LongWritable(totalVisits)));

    }
}
