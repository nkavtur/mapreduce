package home.nkavtur.mapreduce.tags_count;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

/**
 * @author nkavtur
 */
public class JobClient extends Configured implements Tool {
    private static final Logger LOG = Logger.getLogger(JobClient.class);
    private static final String APP_NAME = "Word-Count";

    @Override
    public int run(String[] args) throws Exception {
        LOG.info("Setupping job");
        Job job = Job.getInstance(getConf());
        job.setJarByClass(this.getClass());
        job.setJobName(APP_NAME);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(LongWritable.class);

        job.setMapperClass(TagCountMapper.class);
        job.setReducerClass(TagCountReducer.class);
        job.setCombinerClass(TagCountReducer.class);

        return job.waitForCompletion(true) ? 1 : 0;
    }

    public static void main(final String[] args) throws Exception {
        int result = ToolRunner.run(new Configuration(), new JobClient(), args);
        System.exit(result);
    }
}
