package home.nkavtur.mapreduce.tags_count;

import static home.nkavtur.mapreduce.utils.Constants.FIELDS_NUMBER;
import static home.nkavtur.mapreduce.utils.Constants.TAB;
import static home.nkavtur.mapreduce.utils.Constants.USER_TAG_ID_NUMBER;
import static home.nkavtur.mapreduce.utils.CsvConverter.fromCsv;
import static java.lang.String.format;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.Map;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Logger;

import home.nkavtur.mapreduce.utils.UserProfileTagsEntry;

/**
 * @author nkavtur
 */
public class TagCountMapper extends Mapper<Object, Text, Text, LongWritable>{
    private static final Logger LOG = Logger.getLogger(TagCountMapper.class);
    private static final LongWritable ONE = new LongWritable(1);
    private Map<String, UserProfileTagsEntry> userProfileTags;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
        URI uri = context.getCacheFiles()[0];
        FileInputStream input = new FileInputStream(new File(new Path(uri).getName()));
        userProfileTags = fromCsv(input).stream().collect(toMap(UserProfileTagsEntry::getId, identity()));
    }

    @Override
    protected void map(Object key, Text line, Context context)
            throws IOException, InterruptedException {
        String[] tokens = line.toString().split(TAB);
        if (isNotValid(tokens)) {
            LOG.info(format("Line [%s] is not valid. Has size of [%s] but [%s]", line, tokens.length, FIELDS_NUMBER));
            return;
        }

        String userTagId = tokens[USER_TAG_ID_NUMBER];
        UserProfileTagsEntry userProfileTag = userProfileTags.get(userTagId);
        if (userProfileTag == null) {
            return;
        }

        for (String tag: userProfileTag.getValue()) {
            LOG.debug(format("Emitting key=[%s], value=1", tag));
            context.write(new Text(tag), ONE);
        }
    }

    boolean isNotValid(String[] tokens) {
        return tokens.length != FIELDS_NUMBER;
    }
}
