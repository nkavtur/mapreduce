package home.nkavtur.mapreduce.tags_count;

import static java.util.stream.StreamSupport.stream;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * @author nkavtur
 */
public class TagCountReducer extends Reducer<Text, LongWritable, Text, LongWritable>{

    @Override
    protected void reduce(Text key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {
        long sum = stream(values.spliterator(), false).mapToLong(LongWritable::get).sum();
        context.write(key, new LongWritable(sum));
    }
}
