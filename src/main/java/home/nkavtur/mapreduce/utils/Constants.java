package home.nkavtur.mapreduce.utils;

/**
 * @author nkavtur
 */
public final class Constants {
    public static final String TAB =                            "\\t";
    public static final int FIELDS_NUMBER =                     22;
    public static final int USER_TAG_ID_NUMBER =                20;
    public static final int PRICE_NUMBER =                      18;
    public static final int IP_NUMBER =                         4;
    public static final int USER_AGENT_FIELD_NUMBER =           3;
    public static final int TIMESTAMP_FIELD_NUMBER =            1;
    public static final int IPINYOUID_FIELD_NUMBER =            2;
    public static final int STREAM_ID_FIELD_NUMBER =            21;
    public static final int IMPRESSION_SITE_VALUE =             1;
}
