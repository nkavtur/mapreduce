package home.nkavtur.mapreduce.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

/**
 * @author nkavtur
 */
public final class CsvConverter {
    private static final String[] HEADERS = { "KeywordId", "KeywordValue", "KeywordStatus", "pricingType",
            "KeywordMatchType", "destinationUrl" };
    private static final CSVFormat FORMAT = CSVFormat.DEFAULT.withRecordSeparator("\n");

    @SuppressWarnings("rawtypes")
    public static String toCsv(List<UserProfileTagsEntry> entries) throws IOException {

        String targetCsv = null;
        try (StringWriter writer = new StringWriter();
            CSVPrinter csvFilePrinter = new CSVPrinter(writer, FORMAT)) {
            csvFilePrinter.printRecord(HEADERS);
            for (UserProfileTagsEntry tag : entries) {
                List record = new ArrayList();
                record.add(tag.getId());
                record.add(Joiner.on(";").join(tag.getValue()));
                record.add(tag.getStatus());
                record.add(tag.getPricingType());
                record.add(tag.getMatchType());
                record.add(tag.getUrl());
                csvFilePrinter.printRecord(record);
            }
            targetCsv = writer.toString();
        }

        return targetCsv;
    }

    @SuppressWarnings("rawtypes")
    public static List<UserProfileTagsEntry> fromCsv(InputStream input) throws IOException {
        List<UserProfileTagsEntry> entries = Lists.newArrayList();
        try(CSVParser parser = new CSVParser(new InputStreamReader(input), FORMAT)) {
            List<CSVRecord> records = parser.getRecords();
            for (int i = 0; i < records.size(); i++) {
                if (i == 0) continue;
                CSVRecord record = records.get(i);
                entries.add(new UserProfileTagsEntry().
                        withId(record.get(0)).
                        withValue(Lists.newArrayList(Splitter.on(";").split(record.get(1)))).
                        withStatus(record.get(2)).
                        withPricingType(record.get(3)).
                        withMatchType(record.get(4)).
                        withUrl(record.get(5)));
            }
        }

        return entries;
    }

}
