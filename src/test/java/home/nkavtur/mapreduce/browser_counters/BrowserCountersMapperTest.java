package home.nkavtur.mapreduce.browser_counters;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import static java.nio.charset.Charset.*;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

import com.google.common.io.Files;

import static com.google.common.io.Files.*;

/**
 * @author nkavtur
 */
public class BrowserCountersMapperTest {
    private MapDriver<Object, Text, NullWritable, NullWritable> mapDriver;
    private File validDataset = new File("src/test/resources/dataset.txt");
    private File invalidDataset = new File("src/test/resources/invalidDataset.txt");
    private String chrome = "mozilla/5.0 (windows nt 6.1; wow64) applewebkit/537.36 (khtml, like gecko) chrome/28.0.1478.0 safari/537.36";
    private String ie = "mozilla/4.0 (compatible; msie 6.0; windows nt 5.1; sv1)";
    private String firefox = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1";
    private String other = "someOtherValue";

    @Before
    public void setup() throws IOException {
        BrowserCountersMapper mapper = new BrowserCountersMapper();
        mapDriver = MapDriver.newMapDriver(mapper);
    }

    @Test
    public void firefox() throws IOException {
        // Given
        Text someKey = new Text();
        String line = setUserAgent(readFirstLine(validDataset, defaultCharset()), firefox);

        // When
        mapDriver.setInput(someKey, new Text(line));

        // Then
        mapDriver.runTest();
        assertEquals(1L, mapDriver.getCounters().findCounter(BrowserType.FIREFOX).getValue());
    }

    @Test
    public void chrome() throws IOException {
        // Given
        Text someKey = new Text();
        String line = setUserAgent(readFirstLine(validDataset, defaultCharset()), chrome);

        // When
        mapDriver.setInput(someKey, new Text(line));

        // Then
        mapDriver.runTest();
        assertEquals(1L, mapDriver.getCounters().findCounter(BrowserType.CHROME).getValue());
    }

    @Test
    public void ie() throws IOException {
        // Given
        Text someKey = new Text();
        String line = setUserAgent(readFirstLine(validDataset, defaultCharset()), ie);

        // When
        mapDriver.setInput(someKey, new Text(line));

        // Then
        mapDriver.runTest();
        assertEquals(1L, mapDriver.getCounters().findCounter(BrowserType.IE).getValue());
    }

    @Test
    public void other() throws IOException {
        // Given
        Text someKey = new Text();
        String line = setUserAgent(readFirstLine(validDataset, defaultCharset()), other);

        // When
        mapDriver.setInput(someKey, new Text(line));

        // Then
        mapDriver.runTest();
        assertEquals(1L, mapDriver.getCounters().findCounter(BrowserType.OTHERS).getValue());
    }

    @Test
    public void map_WhenLineSizeIsNotExpected_thenSkip() throws IOException {
        //Given
        Text someKey = new Text(); 
        String line = setUserAgent(Files.readFirstLine(invalidDataset, Charset.defaultCharset()) , other);

        // When
        mapDriver.setInput(someKey, new Text(line));

        // Then
        mapDriver.runTest();
    }

    private String setUserAgent(String line, String userAgent) {
        return String.format(line, userAgent, "someValue", "someValue");
    }
}
