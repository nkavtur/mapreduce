package home.nkavtur.mapreduce.secondary_sort;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

import com.google.common.io.Files;

public class SecondarySortMapperTest {
    private MapDriver<Object, Text, CompositeKey, LineWithImpression> mapDriver;
    private File dataset1 = new File("src/test/resources/test_dataset1.txt");
    private File dataset2 = new File("src/test/resources/test_dataset2.txt");
    private File dataset3 = new File("src/test/resources/test_dataset3.txt");
    private File dataset4 = new File("src/test/resources/test_dataset4.txt");

    private File invalidDataset = new File("src/test/resources/invalidDataset.txt");

    @Before
    public void setup() throws IOException {
        SecondarySortMapper mapper = new SecondarySortMapper();
        mapDriver = MapDriver.newMapDriver(mapper);
    }

    @Test
    public void map_WhenSiteImpression() throws IOException {
        //Given
        Text someKey = new Text(); 
        String line = Files.readFirstLine(dataset1, Charset.defaultCharset());

        // When
        mapDriver.setInput(someKey, new Text(line));

        CompositeKey key = new CompositeKey().withIPinyouId("test1").withTimestamp(20130606000112501L);
        LineWithImpression value = new LineWithImpression().withLine(line).withSiteImpression(true);
        mapDriver.addOutput(key, value);

        // Then
        mapDriver.runTest();
    }

    @Test
    public void map_WhenNotSiteImpression() throws IOException {
        //Given
        Text someKey = new Text(); 
        String line = Files.readFirstLine(dataset2, Charset.defaultCharset());

        // When
        mapDriver.setInput(someKey, new Text(line));

        CompositeKey key = new CompositeKey().withIPinyouId("test1").withTimestamp(20130606000112501L);
        LineWithImpression value = new LineWithImpression().withLine(line).withSiteImpression(false);
        mapDriver.addOutput(key, value);

        // Then
        mapDriver.runTest();
    }

    @Test
    public void map_WhenIpinyouIdNull_thenSkip() throws IOException {
        //Given
        Text someKey = new Text(); 
        String line = Files.readFirstLine(dataset3, Charset.defaultCharset());

        // When
        mapDriver.setInput(someKey, new Text(line));

        // Then
        mapDriver.runTest();
    }

    @Test
    public void map_WhenStreamIdIsNotNumber_thenConsiderItZero() throws IOException {
        //Given
        Text someKey = new Text(); 
        String line = Files.readFirstLine(dataset4, Charset.defaultCharset());

        // When
        mapDriver.setInput(someKey, new Text(line));
        CompositeKey key = new CompositeKey().withIPinyouId("test1").withTimestamp(20130606000112501L);
        LineWithImpression value = new LineWithImpression().withLine(line).withSiteImpression(false);
        mapDriver.addOutput(key, value);

        // Then
        mapDriver.runTest();
    }

    @Test
    public void map_WhenLineSizeIsNotExpected_thenSkip() throws IOException {
        //Given
        Text someKey = new Text(); 
        String line = Files.readFirstLine(invalidDataset, Charset.defaultCharset());

        // When
        mapDriver.setInput(someKey, new Text(line));

        // Then
        mapDriver.runTest();
    }
}
