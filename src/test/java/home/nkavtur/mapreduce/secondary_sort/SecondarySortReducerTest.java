package home.nkavtur.mapreduce.secondary_sort;

import static java.util.Arrays.asList;
import static org.apache.hadoop.mrunit.mapreduce.ReduceDriver.newReduceDriver;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;
import org.apache.hadoop.mrunit.types.Pair;

/**
 * @author nkavtur
 */
public class SecondarySortReducerTest {
    private ReduceDriver<CompositeKey, LineWithImpression, Text, Text> reduceDriver;

    @Before
    public void setup() throws IOException {
        SecondarySortReducer reducer = new SecondarySortReducer();
        reduceDriver = newReduceDriver(reducer);
    }

    @Test
    public void reduce() throws IOException {
        // Given
        CompositeKey key1 = new CompositeKey().withIPinyouId("someId1").withTimestamp(123L);
        LineWithImpression value11 = new LineWithImpression().withLine("someLine11").withSiteImpression(true);
        LineWithImpression value12 = new LineWithImpression().withLine("someLine12").withSiteImpression(false);
        LineWithImpression value13 = new LineWithImpression().withLine("someLine13").withSiteImpression(true);

        CompositeKey key2 = new CompositeKey().withIPinyouId("someId2").withTimestamp(123L);
        LineWithImpression value21 = new LineWithImpression().withLine("someLine21").withSiteImpression(true);

        // When
        reduceDriver.withAll(asList(
                new Pair(key1, asList(value11, value12, value13)),
                new Pair(key2, asList(value21))));
        reduceDriver.withAllOutput(asList( 
                new Pair(new Text("someLine11"), new Text()), 
                new Pair(new Text("someLine12"), new Text()),
                new Pair(new Text("someLine13"), new Text()),
                new Pair(new Text("someLine21"), new Text()),
                new Pair(new Text("someId1"), new Text("2"))));

        // Then
        reduceDriver.runTest();
    }
}
