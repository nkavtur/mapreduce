package home.nkavtur.mapreduce.spends_and_visits;

import static home.nkavtur.mapreduce.spends_and_visits.SpendsAndVisits.newInstance;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

import com.google.common.io.Files;

import home.nkavtur.mapreduce.spends_and_visits.SpendsAndVisits;
import home.nkavtur.mapreduce.spends_and_visits.SpendsAndVisitsMapper;

/**
 * @author nkavtur
 */
public class SpendsAndVisitsMapperTest {
    private static final LongWritable ONE = new LongWritable(1);

    private MapDriver<Object, Text, Text, SpendsAndVisits> mapDriver;
    private File validDataset = new File("src/test/resources/dataset.txt");
    private File invalidDataset = new File("src/test/resources/invalidDataset.txt");

    @Before
    public void setup() throws IOException {
        SpendsAndVisitsMapper mapper = new SpendsAndVisitsMapper();
        mapDriver = MapDriver.newMapDriver(mapper);
    }

    @Test
    public void mapValid() throws IOException {
        //Given
        Text someKey = new Text(); 
        String line = setValuesToLine(Files.readFirstLine(validDataset, Charset.defaultCharset()) , "125.34.53.*", "100");

        // When
        mapDriver.setInput(someKey, new Text(line));
        mapDriver.addOutput(new Text("125.34.53.*"), newInstance().withSpends(new LongWritable(100)).withVisits(ONE));

        // Then
        mapDriver.runTest();
    }

    @Test
    public void map_WhenLineSizeIsNotExpected_thenSkip() throws IOException {
        //Given
        Text someKey = new Text(); 
        String line = setValuesToLine(Files.readFirstLine(invalidDataset, Charset.defaultCharset()) , "125.34.53.*", "100");

        // When
        mapDriver.setInput(someKey, new Text(line));

        // Then
        mapDriver.runTest();
    }

    @Test
    public void map_WhenPriceFormatInvalid_thenConsiderItZero() throws IOException {
        //Given
        Text someKey = new Text(); 
        String line = setValuesToLine(Files.readFirstLine(validDataset, Charset.defaultCharset()) , "125.34.53.*", "someValue");

        // When
        mapDriver.setInput(someKey, new Text(line));
        mapDriver.addOutput(new Text("125.34.53.*"), newInstance().withSpends(new LongWritable(0)).withVisits(ONE));

        // Then
        mapDriver.runTest();
    }

    private String setValuesToLine(String line, String ip, String price) {
        return String.format(line, "someValue", ip, price);
    }
}
