package home.nkavtur.mapreduce.spends_and_visits;

import static home.nkavtur.mapreduce.spends_and_visits.SpendsAndVisits.newInstance;

import java.io.IOException;
import java.util.Arrays;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import home.nkavtur.mapreduce.spends_and_visits.SpendsAndVisits;
import home.nkavtur.mapreduce.spends_and_visits.SpendsAndVisitsReducer;

/**
 * @author nkavtur
 */
public class SpendsAndVisitsReducerTest {
    private ReduceDriver<Text, SpendsAndVisits, Text, SpendsAndVisits> reduceDriver;

    @Before
    public void setup() throws IOException {
        SpendsAndVisitsReducer reducer = new SpendsAndVisitsReducer();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    @Test
    public void reduce() throws IOException {
        //Given
        Text ip = new Text("125.34.53.*"); 
        SpendsAndVisits value1 = newInstance().withSpends(new LongWritable(100)).withVisits(new LongWritable(1));
        SpendsAndVisits value2 = newInstance().withSpends(new LongWritable(200)).withVisits(new LongWritable(1));

        // When
        reduceDriver.setInput(ip, Arrays.asList(value1, value2));
        reduceDriver.addOutput(ip, newInstance().withSpends(new LongWritable(300)).withVisits(new LongWritable(2)));

        // Then
        reduceDriver.runTest();
    }
}