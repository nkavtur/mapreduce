package home.nkavtur.mapreduce.tags_count;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

import com.google.common.io.Files;

/**
 * @author nkavtur
 */
public class TagsCountMapperTest {
    private MapDriver<Object, Text, Text, LongWritable> mapDriver;
    private File validDataset = new File("src/test/resources/dataset.txt");
    private File validDataset1 = new File("src/test/resources/dataset1.txt");
    private File invalidDataset = new File("src/test/resources/invalidDataset.txt");

    @Before
    public void setup() throws IOException {
        TagCountMapper mapper = new TagCountMapper();
        mapDriver = MapDriver.newMapDriver(mapper);
    }

    @Test
    public void mapValid() throws IOException {
        //Given
        Text someKey = new Text(); 
        String line = setValuesToLine(Files.readFirstLine(validDataset, Charset.defaultCharset()) , "125.34.53.*", "100");

        // When
        mapDriver.setInput(someKey, new Text(line));
        mapDriver.addOutput(new Text("usd"), new LongWritable(1));
        mapDriver.addOutput(new Text("&"), new LongWritable(1));
        mapDriver.addOutput(new Text("and"), new LongWritable(1));
        mapDriver.addOutput(new Text("the"), new LongWritable(1));

        // Then
        mapDriver.addCacheFile("res.csv");
        mapDriver.runTest();
    }

    @Test
    public void map_WhenLineSizeIsNotExpected_thenSkip() throws IOException {
        //Given
        Text someKey = new Text(); 
        String line = setValuesToLine(Files.readFirstLine(invalidDataset, Charset.defaultCharset()) , "someValue", "SomeValue");

        // When
        mapDriver.setInput(someKey, new Text(line));

        // Then
        mapDriver.addCacheFile("res.csv");
        mapDriver.runTest();
    }

    @Test
    public void map_WhenNoTagsForUserTagId_thenSkip() throws IOException {
        //Given
        Text someKey = new Text(); 
        String line = setValuesToLine(Files.readFirstLine(validDataset1, Charset.defaultCharset()) , "someValue", "SomeValue");

        // When
        mapDriver.setInput(someKey, new Text(line));

        // Then
        mapDriver.addCacheFile("res.csv");
        mapDriver.runTest();
    }

    private String setValuesToLine(String line, String ip, String price) {
        return String.format(line, "someValue", ip, price);
    }
}
