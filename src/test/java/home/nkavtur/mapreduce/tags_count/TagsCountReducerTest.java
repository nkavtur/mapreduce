package home.nkavtur.mapreduce.tags_count;

import static java.util.Arrays.asList;
import static org.apache.hadoop.mrunit.mapreduce.ReduceDriver.newReduceDriver;

import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;
import home.nkavtur.mapreduce.tags_count.TagCountReducer;

/**
 * @author nkavtur
 */
public class TagsCountReducerTest {
    private ReduceDriver<Text, LongWritable, Text, LongWritable> reduceDriver;

    @Before
    public void setup() throws IOException {
        TagCountReducer reducer = new TagCountReducer();
        reduceDriver = newReduceDriver(reducer);
    }

    @Test
    public void reduce() throws IOException {
        // Given
        Text tag = new Text("usd");

        // When
        reduceDriver.setInput(tag, asList(new LongWritable(100), new LongWritable(200)));
        reduceDriver.addOutput(tag, new LongWritable(300));

        // Then
        reduceDriver.runTest();
    }
}